import http from 'http'
import { getRoute } from './controller.js'

const PORT = process.env.PORT || 5000

const successFun = (res, route) => {
  res.writeHead(200, { 'Content-Type': 'application/json' })
  res.end(JSON.stringify(route))
}
const errorFun = (res, code, message) => {
  res.writeHead(code || 404, { 'Content-Type': 'application/json' })
  res.end(JSON.stringify({ message: message || 'Route not found' }))
}

const server = http.createServer(async (req, res) => {
  const url = req.url.split('/')

  if (url[1] === 'routing' && req.method === 'GET') {
    const { data, success, message, code } = await getRoute({
      origin: url[2],
      destination: url[3],
    })

    data ? successFun(res, data) : errorFun(res, code, message)
  } else {
    errorFun(res)
  }
})

server.listen(PORT, () => {
  console.log(`server started on port: ${PORT}`)
})
