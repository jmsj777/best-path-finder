import fs from 'fs'

const cleanDataPath = './data/clean.json'
const countriesPath = './data/countries.json'

const convertToGraph = dirty =>
  dirty.reduce((obj, { cca3, borders }, index) => {
    obj[cca3] = borders
    return obj
  }, {})

export const getCleanData = async () => {
  try {
    if (fs.existsSync(cleanDataPath)) {
      const cleanedData = fs.readFileSync(cleanDataPath)
      return JSON.parse(cleanedData)
    } else {
      const dirty = JSON.parse(fs.readFileSync(countriesPath, 'utf8'))
      const cleanedData = convertToGraph(dirty)

      fs.writeFileSync(cleanDataPath, JSON.stringify(cleanedData))

      return cleanedData
    }
  } catch (err) {
    console.error(err)
    return
  }
}
