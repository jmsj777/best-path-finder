import Graph from './model/Graph.js'

export const getRoute = async ({ origin, destination }) => {
  const graph = await Graph.getInstance()
  if (origin in graph.AdjList && destination in graph.AdjList) {
    const shortestPath = graph.findShortestPath(origin, destination)

    return shortestPath.distance !== Infinity
      ? { data: { route: shortestPath.path } }
      : { code: 400, message: 'No land crossing available' }
  } else
    return {
      code: 404,
      message: 'Please insert only valid cca3 values',
    }
}
