import { getCleanData } from '../utils.js'
class Graph {
  constructor(graph) {
    this.AdjList = graph
  }

  shortestDistanceNode(distances, visited) {
    let shortest = null

    for (let node in distances) {
      let currentIsShortest =
        shortest === null || distances[node] < distances[shortest]
      if (currentIsShortest && !visited.includes(node)) {
        shortest = node
      }
    }
    return shortest
  }

  toUnitaryObj(list) {
    return list.reduce((obj, e) => {
      obj[e] = 1
      return obj
    }, {})
  }

  findShortestPath(startNode, endNode) {
    const distances = this.toUnitaryObj(this.AdjList[startNode])
    distances[endNode] = Infinity

    let parents = { endNode: null }
    for (let child in this.AdjList[startNode]) {
      parents[child] = startNode
    }

    let visited = []

    let node = this.shortestDistanceNode(distances, visited)

    while (node) {
      let distance = distances[node]
      let children = this.toUnitaryObj(this.AdjList[node])

      for (let child in children) {
        if (String(child) === String(startNode)) {
          continue
        } else {
          let newDistance = distance + 1

          if (!distances[child] || distances[child] > newDistance) {
            distances[child] = newDistance
            parents[child] = node
          } else {
          }
        }
      }

      visited.push(node)

      node = this.shortestDistanceNode(distances, visited)
    }

    let shortestPath = [endNode]
    let parent = parents[endNode]
    while (parent) {
      shortestPath.push(parent)
      parent = parents[parent]
    }
    shortestPath.push(startNode)
    shortestPath.reverse()

    return {
      distance: distances[endNode],
      path: shortestPath,
    }
  }

  static async createInstance(d) {
    const data = await getCleanData()
    return new Graph(data)
  }

  static async getInstance() {
    if (!this.instance) {
      this.instance = await this.createInstance()
    }
    return this.instance
  }
}

export default Graph
